import React, { ReactNode, useEffect } from 'react';
import Footer from '../../components/Footer/Footer';
import Header from '../../components/Header/Header';
import { useAppDispatch } from '../../redux/store';
import { fetchProducts } from '../../redux/slices/createAsyncThunk';
import './MainLayout.scss';
import SearchPage from '../../pages/SearchPage';
import { isSearchSelector, updateProductsSelector } from '../../redux/slices/selectors';
import { useSelector } from 'react-redux';

interface MainLayoutProps {
  children: ReactNode;
}

const MainLayout: React.FC<MainLayoutProps> = ({ children }) => {
  const dispatch = useAppDispatch();
  const isSearch = useSelector(isSearchSelector);
  const updateProducts = useSelector(updateProductsSelector);

  useEffect(() => {
    if (updateProducts) dispatch(fetchProducts());
  }, [updateProducts]);

  return (
    <div className="wrapper">
      <Header />
      <div className="content">{children}</div>
      <Footer />
      {isSearch && <SearchPage />}
    </div>
  );
};

export default MainLayout;
