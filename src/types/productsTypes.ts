export type AllProductsType = {
  [key: string]: {
    [key: string]: ProductType;
  };
};

export type ProductType = {
  category: string;
  name: string;
  description: string;
  price: number;
  quantity: number;
  likes: number;
  photo: string;
  id: string;
};
