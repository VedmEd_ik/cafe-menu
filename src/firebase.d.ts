export declare module './firebase' {
  import { FirebaseApp } from 'firebase/app';

  const appFirebase: FirebaseApp;

  export { appFirebase };
}
