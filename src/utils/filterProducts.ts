import { AllProductsType, ProductType } from '../types/productsTypes';

export default function filterProducts(
  allProducts: AllProductsType,
  filterValue: string,
): ProductType[] {
  const result = [];

  for (const category in allProducts) {
    result.push(...Object.values(allProducts[category]));
  }

  return result.filter((product) => {
    const name = product.name.toLowerCase();
    return name.includes(filterValue.toLowerCase());
  });
}
