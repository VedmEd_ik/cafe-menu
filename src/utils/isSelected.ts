import { ProductType } from '../types/productsTypes';

export default function isSelected(nameProduct: string, selectedProductsArr: ProductType[]) {
  return (
    selectedProductsArr.filter((currentProduct) => nameProduct === currentProduct.name).length > 0
  );
}
