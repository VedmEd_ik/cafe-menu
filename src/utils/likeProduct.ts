import { likedProductsLocalStorage } from '../assets/constants';

export const checkIsLikeProduct = (nameProduct: string) => {
  const likedProducts: string[] = JSON.parse(
    localStorage.getItem(likedProductsLocalStorage) || '[]',
  );

  return likedProducts.includes(nameProduct) ? true : false;
};
