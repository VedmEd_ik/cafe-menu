import { AllProductsType, ProductType } from '../types/productsTypes';

// Функція для отримання відсортованого масиву продуктів за кількістю лайків
export default function sortProductsByLikes(data: AllProductsType): ProductType[] {
  const sortedProducts = [];

  // Цикл по категоріях (чай, кава, десерти)
  for (const category in data) {
    if (data.hasOwnProperty(category)) {
      const products = data[category];

      // Цикл по продуктам у кожній категорії
      for (const productName in products) {
        if (products.hasOwnProperty(productName)) {
          const productDetails = products[productName];
          // Додаємо продукт до масиву
          sortedProducts.push(productDetails);
        }
      }
    }
  }

  // Сортуємо масив за кількістю лайків у зворотньому порядку (від більшої кількості до меншої)
  sortedProducts.sort((a, b) => b.likes - a.likes);
  return sortedProducts;
}
