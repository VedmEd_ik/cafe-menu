import { AllProductsType, ProductType } from '../types/productsTypes';

export default function objectToArray(obj: AllProductsType, categoryName: string): ProductType[] {
  const productsArr = Object.values(obj[categoryName.substring(1)]);
  return productsArr;
}
