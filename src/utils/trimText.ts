export default function trimText(text: string, maxLength: number = 100) {
  if (text.length <= maxLength) {
    return text; // Не потрібно обрізати, якщо текст вже коротший або рівний встановленій довжині.
  } else {
    return text.slice(0, maxLength) + '...';
  }
}
