export const pathNames = {
  categories: { coffe: '/coffe', tea: '/tea', desserts: '/desserts' },
  product: '/product',
  favorite: '/favorite',
  selected: '/selected',
  contacts: '/contacts',
};

export const keyDataLocalStorage = 'cafe-menu';

export const likedProductsLocalStorage = 'cafe-menu-liked-products';
