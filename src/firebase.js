import { initializeApp } from "firebase/app";

const firebaseConfig = {
	apiKey: "AIzaSyBXjYAw-ZvwA9M7w7h3gF1B9VcrSFKCHhU",
	authDomain: "menu-cafe-a107d.firebaseapp.com",
	projectId: "menu-cafe-a107d",
	storageBucket: "menu-cafe-a107d.appspot.com",
	messagingSenderId: "913002120822",
	appId: "1:913002120822:web:b745a902eea20a0b054157",
	measurementId: "G-S8L1JLVDD8",
	databaseURL: 'https://menu-cafe-a107d-default-rtdb.europe-west1.firebasedatabase.app'

};

// Initialize Firebase
export const appFirebase = initializeApp(firebaseConfig);