import React from 'react';
import ReactDOM from 'react-dom/client';
import { createBrowserRouter, Navigate, RouterProvider } from 'react-router-dom';
import { store } from './redux/store.ts';
import { Provider } from 'react-redux';
import MainPage from './pages/MainPage.tsx';
import ProductPage from './pages/ProductPage.tsx';
import FavoritePage from './pages/FavoritePage.tsx';
import SelectedPage from './pages/SelectedPage.tsx';
import './styles/index.scss';
import ContactsPage from './pages/ContactsPage.tsx';
import { pathNames } from './assets/constants.ts';
import SearchPage from './pages/SearchPage.tsx';

const router = createBrowserRouter([
  {
    path: pathNames.categories.coffe,
    element: <MainPage url={pathNames.categories.coffe} />,
  },
  {
    path: pathNames.categories.tea,
    element: <MainPage url={pathNames.categories.tea} />,
  },
  {
    path: pathNames.categories.desserts,
    element: <MainPage url={pathNames.categories.desserts} />,
  },
  {
    path: pathNames.product,
    element: <ProductPage />,
  },
  {
    path: pathNames.favorite,
    element: <FavoritePage />,
  },
  {
    path: pathNames.selected,
    element: <SelectedPage />,
  },
  {
    path: pathNames.contacts,
    element: <ContactsPage />,
  },
  {
    path: '/search',
    element: <SearchPage />,
  },
  {
    path: '*',
    element: <Navigate to={pathNames.categories.coffe} />,
  },
]);

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <Provider store={store}>
      <RouterProvider router={router} />
    </Provider>
  </React.StrictMode>,
);
