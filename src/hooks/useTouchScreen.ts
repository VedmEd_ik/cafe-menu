import React from 'react';

const useTouchScreen = () => {
  const [screen, setScreen] = React.useState<'touch' | 'pc'>('pc');

  const isMobile = {
    Android: function () {
      return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function () {
      return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function () {
      return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function () {
      return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function () {
      return navigator.userAgent.match(/IEMobile/i);
    },
    any: function () {
      return (
        isMobile.Android() ||
        isMobile.BlackBerry() ||
        isMobile.iOS() ||
        isMobile.Opera() ||
        isMobile.Windows()
      );
    },
  };

  React.useEffect(() => {
    if (isMobile.any()) {
      document.body.classList.add('_touch');
      setScreen('touch');
    } else {
      document.body.classList.add('_pc');
      setScreen('pc');
    }
  }, []);

  return { screen };
};

export default useTouchScreen;
