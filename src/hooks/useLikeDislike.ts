import { getDatabase, increment, ref, update } from 'firebase/database';
import { setUpdateProducts } from '../redux/slices/productsSlice';
import { useAppDispatch } from '../redux/store';
import { likedProductsLocalStorage } from '../assets/constants';

const useLikeDislike = () => {
  const dispatch = useAppDispatch();
  const dbRef = ref(getDatabase());

  const like = (name: string, category: string): void => {
    const updates: Record<string, unknown> = {};
    updates[`${category}/${name}/likes`] = increment(1);
    update(dbRef, updates);

    dispatch(setUpdateProducts(true));
  };

  const dislike = (name: string, category: string): void => {
    const updates: Record<string, unknown> = {};
    updates[`${category}/${name}/likes`] = increment(-1);
    update(dbRef, updates);

    dispatch(setUpdateProducts(true));
  };

  const likeDislikeProduct = (nameProduct: string, category: string) => {
    const likedProducts: string[] = JSON.parse(
      localStorage.getItem(likedProductsLocalStorage) || '[]',
    );

    if (!likedProducts || likedProducts.length <= 0) {
      localStorage.setItem(likedProductsLocalStorage, JSON.stringify([nameProduct]));
      like(nameProduct, category);
      return;
    }

    if (likedProducts.includes(nameProduct)) {
      const newArr = likedProducts.filter((name) => name !== nameProduct);
      localStorage.setItem(likedProductsLocalStorage, JSON.stringify(newArr));
      dislike(nameProduct, category);
    } else {
      likedProducts.push(nameProduct);
      localStorage.setItem(likedProductsLocalStorage, JSON.stringify(likedProducts));
      like(nameProduct, category);
    }
  };

  return { likeDislikeProduct };
};

export default useLikeDislike;
