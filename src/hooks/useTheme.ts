import React from 'react';

const isDarkTheme = window?.matchMedia('(prefers-color-scheme: dark)').matches;
const defaultTheme = isDarkTheme ? false : true;

// True = світла тема, false = темна тема

const useTheme = () => {
  const getThemeInLocalStorage = localStorage.getItem('learnWebHub-theme') || defaultTheme;

  const themeInLocalStorage =
    getThemeInLocalStorage === 'false' || !getThemeInLocalStorage ? false : true;

  const [theme, setTheme] = React.useState<boolean>(themeInLocalStorage);

  React.useLayoutEffect(() => {
    if (theme) document.documentElement.setAttribute('data-theme', 'light');
    if (!theme) document.documentElement.setAttribute('data-theme', 'dark');
    localStorage.setItem('learnWebHub-theme', String(theme));
  }, [theme]);

  return { theme, setTheme };
};

export default useTheme;
