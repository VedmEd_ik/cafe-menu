import { useState, useEffect } from 'react';
import { DataSnapshot, child, get, getDatabase, ref } from 'firebase/database';
import { appFirebase } from '../firebase';
import { AllProductsType, ProductType } from '../types/productsTypes';

const useProducts = (url: string) => {
  const [allProducts, setAllProducts] = useState<AllProductsType | undefined>();
  const [productsArr, setProductsArr] = useState<ProductType[]>([]);

  useEffect(() => {
    const fetchData = async () => {
      const dataFromStorage = sessionStorage.getItem('cafe-menu');
      const allProducts: AllProductsType | null = dataFromStorage
        ? JSON.parse(dataFromStorage)
        : null;

      if (allProducts) {
        setAllProducts(allProducts);
        setProductsArr(Object.values(allProducts[url]));
      } else {
        const dbRef = ref(getDatabase(appFirebase));
        try {
          const snapshot: DataSnapshot = await get(child(dbRef, `/`));

          if (snapshot.exists()) {
            const productsObj = snapshot.val();
            // sessionStorage.setItem('cafe-menu', JSON.stringify(productsObj));
            setAllProducts(productsObj);
            setProductsArr(Object.values(productsObj[url]));
          } else {
            console.log('Дані від сервера не отримані');
          }
        } catch (error) {
          console.error('Помилка при отриманні даних від сервера');
          console.error(error);
        }
      }
    };

    fetchData();
  }, [url]);

  return { allProducts, productsArr };
};

export default useProducts;
