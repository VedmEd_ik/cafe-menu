import React, { useEffect } from 'react';
import MainLayout from '../layouts/MainLayout/MainLayout';
import Main from '../components/Main/Main';
import { useSelector } from 'react-redux';
import objectToArray from '../utils/objectToArray';
import { productsSelector, viewProductsSelector } from '../redux/slices/selectors';
import { useAppDispatch } from '../redux/store';
import { setViewProducts } from '../redux/slices/productsSlice';

interface MainPageProps {
  url: string;
}

const MainPage: React.FC<MainPageProps> = ({ url }) => {
  const dispatch = useAppDispatch();
  const products = useSelector(productsSelector);
  const viewProducts = useSelector(viewProductsSelector);

  useEffect(() => {
    if (Object.values(products).length > 0) {
      dispatch(setViewProducts(objectToArray(products, url)));
    }
  }, [products, url]);

  return <MainLayout>{viewProducts && <Main productsArr={viewProducts} />}</MainLayout>;
};

export default MainPage;
