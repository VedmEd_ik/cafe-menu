import React from 'react';
import './SearchPage.scss';
import { useSelector } from 'react-redux';
import { searchProductsSelector } from '../redux/slices/selectors';
import ProductCard from '../components/Product-card/ProductCard';

interface SearchPageProps {}

const SearchPage: React.FC<SearchPageProps> = ({}) => {
  const searchProducts = useSelector(searchProductsSelector);

  return (
    <div className="search-page-wrapper">
      <div className={`search-page  _container `}>
        {searchProducts && searchProducts.length === 0 ? (
          <div className="search-page__none">Нічого не знайдено</div>
        ) : (
          searchProducts &&
          searchProducts.map((product, index) => (
            <ProductCard key={`${index} + ${product.name}`} product={product} />
          ))
        )}
      </div>
    </div>
  );
};

export default SearchPage;
