import React from 'react';
import MainLayout from '../layouts/MainLayout/MainLayout';
import TitlePage from '../components/TitlePage/TitlePage';
import { useSelector } from 'react-redux';
import ProductCard from '../components/Product-card/ProductCard';
import './SelectedPage.scss';
import { selectedProductsSelector } from '../redux/slices/selectors';

interface SelectedPageProps {}

const SelectedPage: React.FC<SelectedPageProps> = ({}) => {
  const selectedProducts = useSelector(selectedProductsSelector);

  return (
    <MainLayout>
      <TitlePage title="Вибрані" />
      <div className="selected _container">
        {selectedProducts.length > 0 ? (
          selectedProducts.map((product, index) => (
            <ProductCard key={`${index} + ${product.name}`} product={product} />
          ))
        ) : (
          <h2 className="selected__none">На жаль, ви ще нічого не обрали. :(</h2>
        )}
      </div>
    </MainLayout>
  );
};

export default SelectedPage;
