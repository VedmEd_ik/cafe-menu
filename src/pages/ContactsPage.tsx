import React from 'react';
import MainLayout from '../layouts/MainLayout/MainLayout';
import TitlePage from '../components/TitlePage/TitlePage';
import './ContactsPage.scss';

interface ContactsPageProps {}

const ContactsPage: React.FC<ContactsPageProps> = ({}) => {
  return (
    <MainLayout>
      <TitlePage title="Контакти" />
      <section className="contacts _container _border">
        <div className="contacts__phone">
          <span className="_icon-phone_1"></span>
          <a href="tel:0969654068" target="_blank" rel="noopener">
            (096) 965-40-68
          </a>
        </div>

        <div className="contacts__instagram">
          <span className="_icon-Instagram_color"></span>
          <a href="https://www.instagram.com/kvarta_kava" target="_blank" rel="noopener">
            kavrta_kava
          </a>
        </div>

        <div className="contacts__location">
          <span className="_icon-location"></span>
          <a href="https://maps.app.goo.gl/bve8eJBuYqKjZcwG7" target="_blank" rel="noopener">
            вул. Шевченка, 47б, м.Дунаївці
          </a>
        </div>
      </section>
    </MainLayout>
  );
};

export default ContactsPage;
