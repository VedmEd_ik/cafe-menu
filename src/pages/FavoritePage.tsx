import React, { useEffect } from 'react';
import MainLayout from '../layouts/MainLayout/MainLayout';
import TitlePage from '../components/TitlePage/TitlePage';
import { useAppDispatch } from '../redux/store';
import { useSelector } from 'react-redux';
import { setFavoriteProducts } from '../redux/slices/productsSlice';
import ProductCard from '../components/Product-card/ProductCard';
import './FavoritePage.scss';
import { fetchProducts } from '../redux/slices/createAsyncThunk';
import { favoriteProductsSelector, productsSelector } from '../redux/slices/selectors';

interface FavoritePageProps {}

const FavoritePage: React.FC<FavoritePageProps> = ({}) => {
  const dispatch = useAppDispatch();
  const products = useSelector(productsSelector);
  const favoriteProducts = useSelector(favoriteProductsSelector);

  if (!products) {
    dispatch(fetchProducts());
  }

  useEffect(() => {
    dispatch(setFavoriteProducts());
  }, [products]);

  return (
    <MainLayout>
      <TitlePage title="Популярні" />
      <div className="favorite _container">
        {favoriteProducts &&
          favoriteProducts.map((product, index) => (
            <ProductCard key={`${index} + ${product.name}`} product={product} />
          ))}
      </div>
    </MainLayout>
  );
};

export default FavoritePage;
