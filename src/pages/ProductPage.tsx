import React from 'react';
import Product from '../components/Product/Product';
import MainLayout from '../layouts/MainLayout/MainLayout';

interface ProductPageProps {}

const ProductPage: React.FC<ProductPageProps> = ({}) => {
  return (
    <MainLayout>
      <Product />
    </MainLayout>
  );
};

export default ProductPage;
