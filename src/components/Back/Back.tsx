import React from 'react';
import './Back.scss';
import { useNavigate } from 'react-router-dom';

interface BackProps {
  classN?: string;
}

const Back: React.FC<BackProps> = ({ classN }) => {
  const navigate = useNavigate();

  return (
    <div className={`back ${classN}`}>
      <span className="back__arrow _icon-arrow" onClick={() => navigate(-1)}></span>
    </div>
  );
};

export default Back;
