import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import trimText from '../../utils/trimText';
import { ProductType } from '../../types/productsTypes';
import { useAppDispatch } from '../../redux/store';
import { selectProduct, unselectProduct } from '../../redux/slices/productsSlice';
import { useSelector } from 'react-redux';
import { selectedProductsSelector } from '../../redux/slices/selectors';
import { checkIsLikeProduct } from '../../utils/likeProduct';
import Likes from '../Likes/Likes';
import useLikeDislike from '../../hooks/useLikeDislike';
import './ProductCard.scss';
import isSelected from '../../utils/isSelected';

interface ProductCardProps {
  key?: string;
  product: ProductType;
}

const ProductCard: React.FC<ProductCardProps> = ({ product }) => {
  const { category, name, description, price, quantity, likes, photo } = product;
  const dispatch = useAppDispatch();
  const [isLiked, setIsLiked] = useState(checkIsLikeProduct(name));
  const { likeDislikeProduct } = useLikeDislike();

  const selectedProducts = useSelector(selectedProductsSelector);

  const handleSelected = () => {
    if (isSelected(product.name, selectedProducts)) {
      dispatch(unselectProduct(product));
    } else {
      dispatch(selectProduct(product));
    }
  };

  const handleClickLike = (name: string, category: string): void => {
    likeDislikeProduct(name, category);
    setIsLiked(checkIsLikeProduct(name));
  };

  return (
    <article className="product-card _border">
      <div className="product-card__action-icons action-icons" onClick={handleSelected}>
        <span
          className={`action-icons__icon ${
            isSelected(product.name, selectedProducts) ? '_icon-minus' : '_icon-plus_circle_fill'
          } `}
        ></span>
      </div>
      <Link to={`/product`} state={{ category, name, description, price, quantity, likes, photo }}>
        <div className="product-card__photo">
          <img src={photo} alt="photo" />
        </div>
        <div className="product-card__main-info">
          <div className="product-card__title">{name}</div>
          <div className="product-card__description">{trimText(description, 45)}</div>
          <div className="product-card__footer">
            <div className="product-card__price">{price} грн.</div>
            <div className="product-card__quantity">{quantity} г</div>
            <div className="product-card__pseudo-likes"></div>
          </div>
        </div>
      </Link>
      <Likes
        classN={`product-card__likes ${isLiked ? '_icon-like' : '_icon-like-out'}`}
        likes={likes}
        clickLike={() => handleClickLike(name, category)}
      />
    </article>
  );
};

export default ProductCard;
