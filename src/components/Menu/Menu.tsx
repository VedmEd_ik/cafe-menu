import React, { useEffect, useState } from 'react';
import './Menu.scss';
import { Link, useLocation } from 'react-router-dom';

type menuItemsType = {
  [key: string]: string;
  Кава: 'coffe';
  Чай: 'tea';
  Десерти: 'desserts';
};

const menuItems: menuItemsType = {
  Кава: 'coffe',
  Чай: 'tea',
  Десерти: 'desserts',
};

const Menu: React.FC = () => {
  const [isSticky, setIsSticky] = useState(false);
  const location = useLocation();
  const url = location.pathname.substring(1);

  useEffect(() => {
    const handleScroll = () => {
      const offset = window.scrollY;
      if (offset > 0) {
        setIsSticky(true);
      } else {
        setIsSticky(false);
      }
    };

    window.addEventListener('scroll', handleScroll);

    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, []);

  return (
    <nav className={`menu _border-bottom _container ${isSticky ? 'sticky' : ''}`}>
      <ul className="menu__list">
        {Object.keys(menuItems).map((item, index) => {
          return (
            <Link to={`/${menuItems[item]}`} key={`item + ${index}`}>
              <li
                className={url === menuItems[item] ? 'menu__item _active-menu-item' : 'menu__item'}
              >
                {item}
              </li>
            </Link>
          );
        })}
      </ul>
    </nav>
  );
};

export default Menu;
