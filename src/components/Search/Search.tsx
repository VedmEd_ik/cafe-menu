import React, { useEffect, useRef, useState } from 'react';
import { useAppDispatch } from '../../redux/store';
import { useSelector } from 'react-redux';
import { productsSelector } from '../../redux/slices/selectors';
import filterProducts from '../../utils/filterProducts';
import { setIsSearch, setSearchProducts } from '../../redux/slices/searchSlice';
import './Search.scss';

interface SearchProps {}

const Search: React.FC<SearchProps> = ({}) => {
  const [isFocus, setIsFocus] = useState(false);
  const [searchValue, setSearchValue] = useState('');
  const products = useSelector(productsSelector);
  const dispatch = useAppDispatch();
  const searchInputRef = useRef<HTMLInputElement | null>(null);
  const body = document.querySelector('body');

  const searchHandler: React.ChangeEventHandler<HTMLInputElement> = (e) => {
    const value = e.target.value;
    setSearchValue(value);
  };

  const handleClickSearch = () => {
    setIsFocus(true);
  };

  useEffect(() => {
    if (isFocus) {
      searchInputRef.current && searchInputRef.current.focus();
      body && body.classList.add('_no-scroll');
      dispatch(setIsSearch(true));
    } else {
      body && body.classList.remove('_no-scroll');
      setSearchValue('');
      dispatch(setIsSearch(false));
    }
  }, [isFocus]);

  useEffect(() => {
    if (!searchValue) {
      dispatch(setIsSearch(false));
      dispatch(setSearchProducts([]));
    } else {
      dispatch(setIsSearch(true));
      dispatch(setSearchProducts(filterProducts(products, searchValue)));
    }
  }, [searchValue]);

  return (
    <>
      <div className={`search _icon-header-container _border ${isFocus ? 'active' : ''}`}>
        <span className="search__icon _icon-search " onClick={handleClickSearch}></span>
        <input
          type="text"
          ref={searchInputRef}
          placeholder=""
          onChange={searchHandler}
          className="search__input"
          value={searchValue}
        />
        <span className="search__close" onClick={() => setIsFocus(false)}></span>
      </div>
    </>
  );
};

export default Search;
