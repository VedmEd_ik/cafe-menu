import React from 'react';
import './Likes.scss';

interface LikesProps {
  likes: number;
  classN: string;
  clickLike?: () => void;
}

const Likes: React.FC<LikesProps> = ({ likes, classN, clickLike }) => {
  return (
    <div className={`likes ${classN}`} onClick={clickLike}>
      <span className="likes__quantity">{likes}</span>
    </div>
  );
};

export default Likes;
