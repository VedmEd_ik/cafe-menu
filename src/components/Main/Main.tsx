import React from 'react';
import Menu from '../Menu/Menu';
import ProductCard from '../Product-card/ProductCard';
import { ProductType } from '../../types/productsTypes';
import './Main.scss';

interface MainProps {
  productsArr: ProductType[];
}
const Main: React.FC<MainProps> = ({ productsArr }) => {
  return (
    <div className="main _border-bottom">
      <Menu />
      <div className="main__content _container">
        {productsArr &&
          productsArr.map((product, index) => (
            <ProductCard key={`${index} + ${product.name}`} product={product} />
          ))}
      </div>
    </div>
  );
};

export default Main;
