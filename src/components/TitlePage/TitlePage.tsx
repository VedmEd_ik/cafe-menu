import React from 'react';
import Back from '../Back/Back';
import './TitlePage.scss';

interface TitlePageProps {
  title: string;
}

const TitlePage: React.FC<TitlePageProps> = ({ title }) => {
  return (
    <section className="title-page">
      <Back classN="title-page__arrow" />
      <h1 className="title-page__title">{title}</h1>
    </section>
  );
};

export default TitlePage;
