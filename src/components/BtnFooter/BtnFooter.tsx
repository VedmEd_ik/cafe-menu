import React from 'react';
import './BtnFooter.scss';
import { Link } from 'react-router-dom';

interface BtnFooterProps {
  icon: string;
  title: string;
  quantity?: number;
  url: string;
}

const BtnFooter: React.FC<BtnFooterProps> = ({ icon, title, quantity, url }) => {
  return (
    <Link to={url} className="btn-footer _border">
      <span className={icon}></span>
      <span className="btn-footer__title">{title}</span>
      {quantity !== undefined && quantity > 0 && (
        <span className="btn-footer__quantity">{quantity}</span>
      )}
    </Link>
  );
};

export default BtnFooter;
