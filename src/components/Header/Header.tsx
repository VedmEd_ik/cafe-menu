import React from 'react';
import './Header.scss';
import Contacts from '../Contacts/Contacts';
import Search from '../Search/Search';
import Logo from '../Logo/Logo';

interface HeaderProps {}

const Header: React.FC<HeaderProps> = ({}) => {
  return (
    <header className="header _container _border-bottom">
      <Search />
      <span></span>
      <Logo />
      <Contacts />
    </header>
  );
};

export default Header;
