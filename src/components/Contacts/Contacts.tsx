import React from 'react';
import { Link } from 'react-router-dom';
import './Contacts.scss';

interface ContactsProps {}

const Contacts: React.FC<ContactsProps> = ({}) => {
  return (
    <div className="contacts-header-icon">
      <Link to={'/contacts'}>
        <span className="_icon-header-container _border _icon-phone_1"></span>
      </Link>
    </div>
  );
};

export default Contacts;
