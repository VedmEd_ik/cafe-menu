import React from 'react';
import logo from '../../assets/images/logo.png';
import './Logo.scss';
import { Link } from 'react-router-dom';

interface LogoProps {}

const Logo: React.FC<LogoProps> = ({}) => {
  return (
    <div className="logo">
      <Link to={'/'}>
        <img src={logo} alt="Логотип" />
        <div>Kvarta</div>
      </Link>
    </div>
  );
};

export default Logo;
