import React from 'react';
import './Footer.scss';
import BtnFooter from '../BtnFooter/BtnFooter';
import { useSelector } from 'react-redux';
import { selectedProductsSelector } from '../../redux/slices/selectors';

interface FooterProps {}

const Footer: React.FC<FooterProps> = ({}) => {
  const selected = useSelector(selectedProductsSelector);

  return (
    <footer className="footer _container">
      <BtnFooter icon="_icon-favorite" title="Популярні" url="/favorite" />
      <BtnFooter
        icon="_icon-plus_circle_fill"
        title="Вибрані"
        quantity={selected.length}
        url="/selected"
      />
    </footer>
  );
};

export default Footer;
