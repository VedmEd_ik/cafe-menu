import React from 'react';
import useTheme from '../../hooks/useTheme';
import cn from 'classnames';
import './SetTheme.scss';

interface SetThemeProps {}

const SetTheme: React.FC<SetThemeProps> = ({}) => {
  const { theme, setTheme } = useTheme();
  const [themeValue, setThemeValue] = React.useState<boolean>(theme);

  React.useEffect(() => {
    setTheme(themeValue);
  }, [themeValue]);

  return (
    <section className="theme-container">
      <div className="theme-buttons">
        <button
          type="button"
          className={cn({ _active: theme }, 'theme-button')}
          onClick={() => setThemeValue(!themeValue)}
        >
          {theme ? (
            <span className="material-symbols-outlined">dark_mode</span>
          ) : (
            <span className="material-symbols-outlined">light_mode</span>
          )}
        </button>
      </div>
    </section>
  );
};

export default SetTheme;
