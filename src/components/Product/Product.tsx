import React, { useState } from 'react';
import Likes from '../Likes/Likes';
import { useLocation } from 'react-router-dom';
import Back from '../Back/Back';
import { checkIsLikeProduct } from '../../utils/likeProduct';
import './Product.scss';

const Product: React.FC = () => {
  let { state } = useLocation();
  const { photo, name, description, quantity, price, likes } = state;
  const [isLiked] = useState(checkIsLikeProduct(name));

  return (
    <section className="product _container">
      <Back />
      <div className="product__photo">
        <img src={photo} alt="photo" />
      </div>
      <div className="product__main-info ">
        <div className="product__title">{name}</div>
        <div className="product__description">
          {description.split('.').map((paragraph: string) => (
            <p key={paragraph.substring(0, 10)}>{paragraph.trim()}</p>
          ))}
        </div>
        <div className="product__footer">
          <div className="product__quantity">Кількість: {quantity} г</div>
          <div className="product__price">Ціна: {price} грн.</div>
          <Likes
            likes={likes}
            classN={`product__likes ${isLiked ? '_icon-like' : '_icon-like-out'}`}
          />
        </div>
      </div>
    </section>
  );
};

export default Product;
