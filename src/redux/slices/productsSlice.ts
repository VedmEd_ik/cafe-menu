import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { AllProductsType, ProductType } from '../../types/productsTypes';
import { fetchProducts } from './createAsyncThunk';
import sortProductsByLikes from '../../utils/sortFavoriteProducts';

export interface ProductsState {
  updateProducts: boolean;
  loading: boolean;
  products: AllProductsType;
  selectedProducts: ProductType[];
  favoriteProducts: ProductType[];
  viewProducts: ProductType[];
}

const initialState: ProductsState = {
  updateProducts: true,
  loading: true,
  products: {},
  selectedProducts: [],
  favoriteProducts: [],
  viewProducts: [],
};

export const productsSlice = createSlice({
  name: 'products',
  initialState,
  reducers: {
    selectProduct(state, action: PayloadAction<ProductType>) {
      state.selectedProducts.push(action.payload);
    },
    unselectProduct(state, action: PayloadAction<ProductType>) {
      state.selectedProducts = state.selectedProducts.filter((product) => {
        return product.name !== action.payload.name;
      });
    },
    setFavoriteProducts(state) {
      state.favoriteProducts = sortProductsByLikes(state.products);
    },
    setViewProducts(state, action: PayloadAction<ProductType[]>) {
      state.viewProducts = action.payload;
    },
    setUpdateProducts(state, action: PayloadAction<boolean>) {
      state.updateProducts = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchProducts.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(fetchProducts.fulfilled, (state, action) => {
      state.products = action.payload;
      state.loading = false;
      state.updateProducts = false;
    });
    builder.addCase(fetchProducts.rejected, (state) => {
      state.loading = true;
      console.log('Помилка сервера');
    });
  },
});

export const {
  selectProduct,
  unselectProduct,
  setFavoriteProducts,
  setViewProducts,
  setUpdateProducts,
} = productsSlice.actions;

export default productsSlice.reducer;
