import { createAsyncThunk } from '@reduxjs/toolkit';
import { AllProductsType } from '../../types/productsTypes';
import { DataSnapshot, child, get, getDatabase, ref } from 'firebase/database';
import { appFirebase } from '../../firebase';
// import { keyDataLocalStorage } from '../../assets/constants';

export const fetchProducts = createAsyncThunk<AllProductsType>(
  'products/fetchProducts',
  async () => {
    console.log('fetch products from database');

    const dbRef = ref(getDatabase(appFirebase));
    try {
      const snapshot: DataSnapshot = await get(child(dbRef, `/`));

      if (snapshot.exists()) {
        const productsObj = snapshot.val();
        sessionStorage.setItem('cafe-menu', JSON.stringify(productsObj));
        return productsObj;
      } else {
        console.log('Дані від сервера не отримані');
      }
    } catch (error) {
      console.error('Помилка при отриманні даних від сервера');
      console.error(error);
    }

    // const dataFromStorage = sessionStorage.getItem(keyDataLocalStorage);
    // if (dataFromStorage) {
    //   return JSON.parse(dataFromStorage);
    // } else {
    //   console.log('fetch');
    //   const dbRef = ref(getDatabase(appFirebase));
    //   try {
    //     const snapshot: DataSnapshot = await get(child(dbRef, `/`));

    //     if (snapshot.exists()) {
    //       const productsObj = snapshot.val();
    //       sessionStorage.setItem('cafe-menu', JSON.stringify(productsObj));
    //       return productsObj;
    //     } else {
    //       console.log('Дані від сервера не отримані');
    //     }
    //   } catch (error) {
    //     console.error('Помилка при отриманні даних від сервера');
    //     console.error(error);
    //   }
    // }
  },
);
