import { RootState } from '../store';

export const productsSelector = (state: RootState) => state.products.products;
export const viewProductsSelector = (state: RootState) => state.products.viewProducts;
export const favoriteProductsSelector = (state: RootState) => state.products.favoriteProducts;
export const selectedProductsSelector = (state: RootState) => state.products.selectedProducts;
export const searchProductsSelector = (state: RootState) => state.search.searchProducts;
export const isSearchSelector = (state: RootState) => state.search.isSearch;
export const updateProductsSelector = (state: RootState) => state.products.updateProducts;
