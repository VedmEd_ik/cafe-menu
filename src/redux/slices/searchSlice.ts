import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { ProductType } from '../../types/productsTypes';

export interface SearchState {
  isSearch: boolean;
  searchProducts: ProductType[];
}

const initialState: SearchState = {
  isSearch: true,
  searchProducts: [],
};

export const searchSlice = createSlice({
  name: 'products',
  initialState,
  reducers: {
    setSearchProducts(state, action: PayloadAction<ProductType[]>) {
      state.searchProducts = action.payload;
    },
    setIsSearch(state, action: PayloadAction<boolean>) {
      state.isSearch = action.payload;
    },
  },
});

export const { setSearchProducts, setIsSearch } = searchSlice.actions;

export default searchSlice.reducer;
